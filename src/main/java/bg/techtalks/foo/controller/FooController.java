package bg.techtalks.foo.controller;

import bg.techtalks.foo.model.Foo;
import bg.techtalks.foo.service.FooService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController()
@RequestMapping("foo")
@Slf4j
public class FooController {

    private FooService fooService;


    @Autowired
    public FooController(FooService fooService) {
        this.fooService = fooService;
    }

    @GetMapping("{id}")
    Foo getById(@PathVariable String id) {

        return fooService.getById(id);
    }
}
