package bg.techtalks.foo.client;

import bg.techtalks.foo.model.Bar;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


@FeignClient(name = "bar", url = "${techtalk.client.url}")
public interface BarClient {

    @GetMapping(value = "/bar")
    List<Bar> getAll();
}
