package bg.techtalks.foo.service;

import bg.techtalks.foo.client.BarClient;
import bg.techtalks.foo.model.Bar;
import bg.techtalks.foo.model.Foo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class FooService {

    private BarClient barClient;

    @Autowired
    public FooService(BarClient barClient) {
        this.barClient = barClient;
    }

    public Foo getById(String id) {

        List<Bar> bars = barClient.getAll();

        Foo f =  new Foo(id, bars);
        log.info("Get all foo {}", f);
        return f;
    }
}
