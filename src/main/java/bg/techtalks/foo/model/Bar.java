package bg.techtalks.foo.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Bar {
    String id;
    String name;
}
