package bg.techtalks.foo.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
public class Foo {

    private String id;

    private List<Bar> bar;
}
