#!/usr/bin/env bash

tag=1.0.2

cd "$(dirname "$0")"
cd ..

echo "Building service"
sh gradlew clean build

echo "Build Docker Image on K8S"
docker build -t didodim/foo:latest -t didodim/foo:${tag} .

echo "Docker Push Image to Docker hub"
docker push didodim/foo:latest
docker push didodim/foo:${tag}

echo "Applying k8s deployments"
kubectl apply -f deployment --record
