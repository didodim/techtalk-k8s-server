FROM openjdk:9-jdk-slim
ADD build/libs/foo*.jar /opt/foo.jar
ENTRYPOINT ["java","-jar","/opt/foo.jar"]
